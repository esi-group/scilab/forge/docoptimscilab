% Copyright (C) 2008-2010 - Consortium Scilab - Digiteo - Michael Baudin
% Copyright (C) 2008-2009 - INRIA - Serge Steer
%
% This file must be used under the terms of the 
% Creative Commons Attribution-ShareAlike 3.0 Unported License :
% http://creativecommons.org/licenses/by-sa/3.0/


\chapter{The Quapro toolbox}
\label{chapter-quapro}

In this chapter, we present the Quapro toolbox, 
which allows to solve linear and quadratic problems.

This toolbox provides linear and linear quadratic programming 
solvers.
The Quapro toolbox was formely a tool directly provided by Scilab.
It has been transformed into a toolbox for license reasons.

The functions provided by this toolbox are presented 
in the figure \ref{fig-scilab-quaprofuns}.

\begin{figure}
\begin{center}
\begin{tabular}{|l|l|}
\hline
\scifun{linpro} & linear programming solver\\
\scifun{quapro} & linear quadratic programming solver\\
\scifun{mps2linpro} & convert lp problem given in MPS format to linpro format\\
\hline
\end{tabular}
\end{center}
\caption{Functions in the Quapro toolbox.}
\label{fig-scilab-quaprofuns}
\end{figure}

The Quapro toolbox is available as an ATOMS 
package since the 29th of July 2010.
To install it, we may use the following statement 
in Scilab (version $\geq$ 5.1).
\lstset{language=scilabscript}
\begin{lstlisting}
atomsInstall("quapro")
\end{lstlisting}
The previous statement connects Scilab to the 
ATOMS web server, downloads the binary file and 
installs it.
It suffices to restart Scilab and the toolbox is ready to use.

The first section is devoted to the \scifun{linpro} function 
while the second section focuses on the \scifun{quapro} function.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Linear optimization}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Mathematical point of view}
This kind of optimization is the minimization of function 
$f(x)$ with $$f(x) = p^T x$$
under:
\begin{itemize}
\item{no constraints}
\item{inequality constraints \Ref{contraint1}}
\item{\textit{or} inequality constraints and bound constraints (\Ref{contraint1} \& \Ref{contraint2})}
\item{\textit{or} inequality constraints, bound constraints and equality constraints (\Ref{contraint1} \& \Ref{contraint2} \& \Ref{contraint3})}.
\end{itemize}

\begin{equation}
\label{contraint1}
C x \leq b
\end{equation}
\begin{equation}
\label{contraint2}
c_i \leq x \leq c_s
\end{equation}
\begin{equation}
\label{contraint3}
C_e x = b_e
\end{equation}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Scilab function}
The \scifun{linpro} function is designed for linear optimization programming. 
For more details about this function, please refer to 
the help page of the linpro function.
This function and associated routines have been written by Cecilia Pola Mendez and 
Eduardo Casas Renteria from the University of Cantabria. 

Please note that the \scifun{linpro} function cannot solve problems based on sparse matrices. 
For this kind of problem, you can use a Scilab toolbox called LIPSOL that gives an 
equivalent of \scifun{linpro} for sparse matrices. 
LIPSOL is available on 
\href{http://www.scilab.org/contrib/displayContribution.php?fileID=813}{
\underline{Scilab web site}
}

The \scifun{linpro} function is based on:
\begin{itemize}
\item some Fortran routines written by the authors of \textit{linpro},
\item some Fortran \textsc{Blas} routines,
\item some Fortran Scilab routines,
\item some Fortran \textsc{Lapack} routines.
\end{itemize}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Example for \scifun{linpro}}
In this section, we present an example of the use of the 
\scifun{linpro} function from the Quapro toolbox.

We search for $\bx \in \RR^6$ which 
minimize a linear optimization problem with 3 
equality constraints and 2 inequality constraints.
The following script defines the input arguments of the 
\scifun{linpro} function.
\lstset{language=scilabscript}
\begin{lstlisting}
//Find x in R^6 such that:
//C1*x = b1  (3 equality constraints i.e me=3)
C1= [1,-1,1,0,3,1;
    -1,0,-3,-4,5,6;
     2,5,3,0,1,0];
b1=[1;2;3];
//C2*x <= b2  (2 inequality constraints)
C2=[0,1,0,1,2,-1;
    -1,0,2,1,1,0];
b2=[-1;2.5];
//with  x between ci and cs:
ci=[-1000;-10000;0;-1000;-1000;-1000];
cs=[10000;100;1.5;100;100;1000];
//and minimize p'*x with
p=[1;2;3;4;5;6]
//No initial point is given: x0='v';
C=[C1;C2]; 
b=[b1;b2] ; 
me=3; 
x0="v";
[x,lagr,f]=linpro(p,C,b,ci,cs,me,x0)
\end{lstlisting}

The previous script produces the following output.
\lstset{language=scilabscript}
\begin{lstlisting}
-->[x,lagr,f]=linpro(p,C,b,ci,cs,me,x0)
 f  =
  - 7706.4681  
 lagr  =
    0.         
    0.         
  - 3.1914894  
  - 7.4893617  
    2.212766   
    0.         
  - 0.7659574  
  - 0.8723404  
  - 0.5531915  
    0.         
    0.         
 x  =
    275.2766   
  - 129.51064  
    1.537D-14  
  -1000.       
    100.       
  - 703.78723  
\end{lstlisting}
The \scivar{lagr} variable stores the vector of the 
Lagrange multipliers. Its various components are associated 
to the constraints of the problem.
We see that the lower bound constraints \#3 and \#4 are active 
since \scivar{lagr(3:4)} is negative.
We also see that the upper bound constraint \#5 is active 
since \scivar{lagr(5)} is positive.
The linear (equality) constraints \#1 to \#3 are active, since 
\scivar{lagr(7:9)} is different from zero.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Linear quadratic optimization}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Mathematical point of view}
This kind of optimization is the minimization of function 
$f(x)$ with $$f(x) = \frac{1}{2} x^T Q x + p^T x$$ under:
\begin{itemize}
\item no constraints,
\item \textit{or} inequality constraints \Ref{contraint1},
\item \textit{or} inequality constraints and bound constraints 
(\Ref{contraint1} and \Ref{contraint2})
\item \textit{or} inequality constraints, bound constraints and 
equality constraints (\Ref{contraint1}, \Ref{contraint2} and 
\Ref{contraint3}).
\end{itemize}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Scilab function}
The \scifun{quapro} function is designed for linear optimization programming.
As opposed to the \scifun{qld} function, the \scifun{quapro} 
function can be used whatever the matrix $Q$ is (i.e. it does not have to be 
positive definite).

For more details about this function, please refer to the 
help page of the \scifun{quapro} function.

The \scifun{quapro} function and associated routines have been written by 
Cecilia Pola Mendez and Eduardo Casas Renteria from the University of Cantabria.

The \scifun{quapro} function cannot solve problems based on sparse matrices.

The \scifun{quapro} function is based on:
\begin{itemize}
\item some Fortran routines written by the authors of \textit{linpro},
\item some Fortran \textsc{Blas} routines,
\item some Fortran Scilab routines,
\item some Fortran \textsc{Lapack} routines.
\end{itemize}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Example for \scifun{quapro}}
In this section, we present an example of the use of the 
\scifun{quapro} function from the Quapro toolbox.

We search for $\bx \in \RR^6$ which 
minimize a linear-quadratic optimization problem with 3 
equality constraints and 2 inequality constraints.
The following script defines the input arguments of the 
\scifun{quapro} function.

\lstset{language=scilabscript}
\begin{lstlisting}
//Find x in R^6 such that:
//C1*x = b1 (3 equality constraints i.e me=3)
C1= [1,-1,1,0,3,1;
    -1,0,-3,-4,5,6;
     2,5,3,0,1,0];
b1=[1;2;3];
//C2*x <= b2 (2 inequality constraints)
C2=[0,1,0,1,2,-1;
    -1,0,2,1,1,0];
b2=[-1;2.5];
//with  x between ci and cs:
ci=[-1000;-10000;0;-1000;-1000;-1000];
cs=[10000;100;1.5;100;100;1000];
//and minimize 0.5*x'*Q*x + p'*x with
p=[1;2;3;4;5;6]; 
Q=eye(6,6);
//No initial point is given;
C=[C1;C2] ;
b=[b1;b2] ;
me=3;
[x,lagr,f]=quapro(Q,p,C,b,ci,cs,me)
\end{lstlisting}

The previous script produces the following output.
\lstset{language=scilabscript}
\begin{lstlisting}
-->[x,lagr,f]=quapro(Q,p,C,b,ci,cs,me)
 f  =
  - 14.843248  
 lagr  =
    0.         
    0.         
    0.         
    0.         
    0.         
    0.         
  - 1.5564027  
  - 0.1698164  
  - 0.7054782  
    0.3091368  
    0.         
 x  =
    1.7975426  
  - 0.3381487  
    0.1633880  
  - 4.9884023  
    0.6054943  
  - 3.1155623  
\end{lstlisting}

We see that only the linear constraints (1 to 4) are active.
Indeed, the components \scivar{lagr(1:6)} are zero, which 
implies that the bounds constraints are not active at optimum.


