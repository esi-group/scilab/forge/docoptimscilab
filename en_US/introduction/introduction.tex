% Copyright (C) 2008-2010 - Consortium Scilab - Digiteo - Michael Baudin
% Copyright (C) 2008-2009 - Consortium Scilab - Digiteo - Vincent Couvert
% Copyright (C) 2008-2009 - INRIA - Serge Steer
%
% This file must be used under the terms of the 
% Creative Commons Attribution-ShareAlike 3.0 Unported License :
% http://creativecommons.org/licenses/by-sa/3.0/


\chapter*{Introduction}
\addcontentsline{toc}{chapter}{Introduction}
This document aims at giving Scilab users a complete overview of 
optimization features in Scilab. 
It is written for Scilab partners needs in OMD project (http://omd.lri.fr/tiki-index.php).
The core of this document is an analysis of 
current Scilab optimization features. In the final part, we give 
a short list of new features which would be interesting to find in Scilab.
Above all the embedded functionalities of 
Scilab itself, some contributions (toolboxes) have been written 
to improve Scilab capabilities. Many of these toolboxes are 
interfaces to optimization libraries, such as FSQP for example.

In this document, we consider optimization problems in which we try to minimize a cost function 
\begin{math}f(x)\end{math} with or without constraints. 
These problems are partly illustrated in figure \ref{optiProblems}. 
Several properties of the problem to solve may be taken into account by 
the numerical algorithms~:
\begin{itemize}
\item The unknown may be a vector of real values or integer values. 
\item The number of unknowns may be small (from 1 to 10 - 100), medium (from 10 to 100 - 1 000) or 
large (from 1 000 - 10 000 and above), leading to dense or sparse linear systems.
\item There may be one or several cost functions (multi-objective optimization).
\item The cost function may be smooth or non-smooth. 
\item There may be constraints or no constraints.
\item The constraints may be bounds constraints, linear or non-linear constraints. 
\item The cost function can be linear, quadratic or a general non linear function. 
\end{itemize}

\begin{figure}
\begin{center}
\includegraphics[width=20cm,angle=90]{introduction/OptimizationProblems.pdf}
\caption{Classes of optimization problems}
\label{optiProblems}
\end{center}
\end{figure}

In this document, we present the following optimization features of 
Scilab.
\begin{itemize}
\item nonlinear optimization with the \scifun{optim} function,
\item quadratic optimization with the \scifun{qpsolve} function,
\item nonlinear least-square optimization with the \scifun{lsqrsolve} function,
\item semidefinite programming with the \scifun{semidef} function,
\item genetic algorithms with the \scifun{optim\_ga} function,
\item simulated annealing with the \scifun{optim\_sa} function,
\item linear matrix inequalities with the \scifun{lmisolver} function,
\item reading of MPS and SIF files with the \scifun{quapro} and \scifun{CUTEr} toolboxes.
\end{itemize}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Scilab v5.2 provides the \scifun{fminsearch} function, which a derivative-free 
algorithm for small problems. 
The \scifun{fminsearch} function is based on the simplex algorithm of Nelder 
and Mead (not to be confused with Dantzig's simplex for 
linear optimization). This unconstrained algorithm does not require the 
gradient of the cost function. It is efficient for small problems, i.e. 
up to 10 parameters and its memory requirement is only $O(n)$.
This algorithm is known to be able to manage "noisy" functions,
i.e. situations where the cost function is the sum of a 
general nonlinear function and a low magnitude function. 
The \scifun{neldermead} component provides three simplex-based algorithms
which allow to solve unconstrained and nonlinearly constrained optimization
problems. It provides an object oriented access to the options.
The \scifun{fminsearch} function is, in fact, a specialized 
use of the \scifun{neldermead} component.
This component is presented in in depth in \cite{BaudinNM2009}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

An analysis of optimization in Scilab, including performance tests, is presented in 
"Optimization with Scilab, present and future" \cite{BaudinSteer2009}.
The following is the abstract of the paper :

"We present in this paper an overview of optimization algorithms 
available in theScilab software. We focus on the user's point of view, 
that is, we have to minimize or maximize an objective function and must 
find a solver suitable for the problem. The aim of this paper is to give 
a simple but accurate view of what problems can be solved by Scilab and 
what behavior can be expected for those solvers. For each solver, we 
analyze the type of problems that it can solve as well as its advantages 
and limitations. In order to compare the respective performances of the 
algorithms, we use the CUTEr library, which is available in Scilab. 
Numerical experiments are presented, which indicates that there is no 
cure-for-all solvers." 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Several existing optimization features are not presented in this document.
In particular, multi-objective optimization is available in Scilab with the 
genetic algorithm component.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The organization of this document is as following.

In the first chapter, we present an overview of optimization tools in Scilab. 
We briefly present nonlinear optimization tools, numerical derivatives, 
linear and quadratic optimization, non-linear least squares and genetic 
algorithms tools. 
We also briefly present the list of projects which are currently under 
development.

In the second chapter, we analyse the flagship of Scilab in terms of nonlinear 
optimization: the \scifun{optim} function. This function allows to solve 
nonlinear optimization problems without constraints or with bound 
constraints. It provides a Quasi-Newton method, a Limited Memory 
BFGS algorithm and a bundle method for non-smooth functions. 
We analyse its features, the management of the cost function, the linear algebra and the 
management of the memory. Then we consider the algorithms which 
are used behind \scifun{optim}, depending on the type of algorithm
and the constraints.

In the chapter 3, we present the \scifun{qpsolve} and \scifun{qp\_solve}
functions which allows to solve quadratic problems. We describe the 
solvers which are used, the memory requirements and the internal design 
of the tool.

The chapter 4 and 5 present non-linear least squares problems 
and semidefinite programming.

The chapter 6 focuses on genetic algorithms. We give a tutorial 
example of the \scifun{optim\_ga} function in the case of the 
Rastrigin function. We also analyse the support functions which allow 
to configure the behavior of the algorithm and describe the algorithm
which is used.

The simulated annealing is presented in chapter 7, which 
gives an overview of the algorithm used in \scifun{optim\_sa}. 
We present an example of use of this method and shows the convergence of the algorithm.
Then we analyse the support functions and present the neighbor functions, 
the acceptance functions and the temperature laws. 
In the final section, we analyse the structure of the algorithm 
used in \scifun{optim\_sa}.

The Linear Matrix Inequality module is presented in chapter 8. 
This tool allows to solve linear matrix inequalities. 
This chapter was written by Nikoukhah, Delebecque and Ghaoui. 
The syntax of the \scifun{lmisolver} function is analysed and 
several examples are analysed in depth.

The chapter 9 focuses on optimization data files managed by Scilab, especially
MPS and SIF files. 

Some optimization features are available in the 
form of toolboxes, the most important of which are the Quapro, 
CUTEr and the Unconstrained Optimization Problems toolboxes. 
These modules are presented in the chapter 10, along with other 
modules including the interface to CONMIN, to FSQP, to LIPSOL, to 
LPSOLVE, to NEWUOA.

The chapter 11 is devoted to missing optimization features in Scilab.

