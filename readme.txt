Documentation : Optimization in Scilab

Abstract

TODO

Author

Copyright (C) 2008-2010 - Consortium Scilab - Digiteo - Michael Baudin
Copyright (C) 2008-2009 - Consortium Scilab - Digiteo - Vincent Couvert
Copyright (C) 2008-2009 - INRIA - Serge Steer

Licence

This document is released under the terms of the Creative Commons Attribution-ShareAlike 3.0 Unported License :

http://creativecommons.org/licenses/by-sa/3.0/

